package vaultconnector

import (
	"encoding/json"
	"fmt"
)

func (p *Parameters) String() string {
	return fmt.Sprintf("\n\tserver:%s\n\tpath:%s\n\tauth-type:%v", p.Server, p.Path, p.Auth)
}

func (p *Parameters) Validate() error {
	if err := p.Auth.Validate(); err != nil {
		return err
	}
	return nil
}

func (p *Parameters) UnmarshalJSON(data []byte) error {
	type Alias Parameters
	aux := Alias{}
	if err := json.Unmarshal(data, &aux); err != nil {
		return err
	}

	*p = Parameters(aux)

	return p.Validate()
}
