package vaultconnector

import (
	"context"
	"fmt"
	"github.com/hashicorp/vault-client-go"
	"github.com/hashicorp/vault-client-go/schema"
	"log"
)

func New(p Parameters) (s *Connector, err error) {
	defer func() {
		if err != nil {
			err = fmt.Errorf("cannot build connector (%v)", err.Error())
		} else {
			log.Printf("created a vault-connector with p : %v", p.String())
		}
	}()

	if err = p.Validate(); err != nil {
		return nil, fmt.Errorf("invalid parameters (%v)", err.Error())
	}
	s = &Connector{par: p}

	s.client, err = vault.New(
		vault.WithAddress(p.Server),
		vault.WithRequestTimeout(p.Timeout),
	)
	if err != nil {
		return nil, fmt.Errorf("error creating connector %v", err.Error())
	}
	return s, nil
}

func (c *Connector) Auth() (err error) {
	defer func() {
		if err != nil {
			err = fmt.Errorf("cannot authenticate using %s (%v)", c.par.Auth, err.Error())
		} else {
			log.Printf("succesfully authenticated with vault (using %s)", c.par.Auth)
		}
	}()
	switch c.par.Auth {
	case Token:
		{
			err = c.client.SetToken(c.par.Token)
			if err != nil {
				return err
			}
		}
	case AppRole:
		{
			var resp, err = c.client.Auth.AppRoleLogin(
				context.Background(),
				schema.AppRoleLoginRequest{
					RoleId:   c.par.RoleId,
					SecretId: c.par.SecretId,
				},
			)
			if err != nil {
				return err
			}
			if err = c.client.SetToken(resp.Auth.ClientToken); err != nil {
				return err
			}
		}
	default:
		return fmt.Errorf("unsupported auth-type (%s)", c.par.Auth)
	}
	return nil
}

func (c *Connector) Write(data SecretData) error {
	if _, err := c.client.Write(context.Background(), c.par.Path,
		SecretData{"data": data}); err != nil {
		return fmt.Errorf("error writing to vault (%v)", err)
	}
	return nil
}

func (c *Connector) Read() (data SecretData, err error) {
	defer func() {
		if err != nil {
			err = fmt.Errorf("read-vault error: cannot read secrets from path %s (%v)", c.par.Path, err.Error())
		} else {
			log.Printf("successfully read secrets from vault")
		}
	}()

	resp, err := c.client.Read(context.Background(), c.par.Path)
	if err != nil {
		return nil, err
	}
	data, ok := resp.Data["data"].(map[string]interface{})
	if !ok {
		return nil, fmt.Errorf("invalid data-type")
	}
	return data, nil
}
