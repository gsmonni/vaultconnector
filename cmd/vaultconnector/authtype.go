package vaultconnector

import (
	"encoding/json"
	"fmt"
)

func (a *VaultAuthType) Validate() error {
	if a == nil || *a == "" {
		return fmt.Errorf("auth-type set to nil/empty")
	}
	switch *a {
	case Token, AppRole:
		return nil
	}
	return fmt.Errorf("invalid/unsupported auth-type (%s)", *a)
}

func (a *VaultAuthType) UnmarshalJSON(data []byte) error {
	type Alias VaultAuthType
	var name Alias

	err := json.Unmarshal(data, &name)
	if err != nil {
		return err
	}
	s := VaultAuthType(name)
	if err = s.Validate(); err != nil {
		return err
	}
	*a = s
	return nil
}
