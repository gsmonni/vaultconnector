package vaultconnector

const (
	Undefined = VaultAuthType("undefined")
	Token     = VaultAuthType("token")
	AppRole   = VaultAuthType("app-role")
)
