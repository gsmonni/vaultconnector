package vaultconnector

import (
	"github.com/hashicorp/vault-client-go"
	"time"
)

type (
	IValidator interface {
		Validate() error
	}

	VaultAuthType string

	SecretData map[string]interface{}
	Parameters struct {
		Server   string        `json:"server"`
		Timeout  time.Duration `json:"timeout"`
		Auth     VaultAuthType `json:"auth"`
		Path     string        `json:"path"`
		Token    string        `json:"token"`
		SecretId string        ` json:"secret-id"`
		RoleId   string        `json:"role-id"`
	}

	Connector struct {
		par    Parameters
		client *vault.Client
	}
)
