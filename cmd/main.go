package main

import (
	"fmt"
	"log"
	"vaultconnector/cmd/vaultconnector"
)

func main() {
	var p vaultconnector.Parameters
	var d vaultconnector.SecretData

	if err := vaultconnector.ReadJson("./data/vault-access.json", &p); err != nil {
		fmt.Println(err.Error())
		if err := vaultconnector.ReadJson("./data/vault-default.json", &p); err != nil {
			log.Fatalf(err.Error())
		}
	}
	if err := vaultconnector.ReadJson("./data/secrets.json", &d); err != nil {
		log.Fatal("cannot read json file (%v)", err)
	}

	if s, err := vaultconnector.New(p); err != nil {
		log.Fatal(err.Error())
	} else {
		if err = s.Auth(); err != nil {
			log.Fatal(err.Error())
		}

		if err := s.Write(d); err != nil {
			log.Fatal(err.Error())
		}
		if data, err := s.Read(); err != nil {
			log.Fatal(err.Error())
		} else {
			fmt.Printf("write-data: %v / read-data %v", d, data)
		}
	}
}
